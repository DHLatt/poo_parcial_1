﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parcial1
{
    public class Monumento : Descubrimiento
    {
        private bool _jeroglificos;
        public bool Jeroglificos {
            get { return _jeroglificos; }
        }

        private string _autoridad;
        public string Autoridad {
            get { return _autoridad; }
        }

        private List<string> _tributoDioses;
        public List<string> TributoDioses
        {
            get { return _tributoDioses; }
        }

        public Monumento(string pais, string ciudad, DateTime fecha,
            bool jeroglificos, string autoridad, List<string> tributoDioses) : base(pais, ciudad, fecha)
        {
            _jeroglificos = jeroglificos;
            _autoridad = autoridad;
            _tributoDioses = tributoDioses;
        }

        public override float ObtenerPresupuestoExcavacion()
        {
            return Presupuesto * 0.15f;
        }

        public override float ObtenerPresupuestoMateriales()
        {
            return Presupuesto * 0.65f;
        }

        public override float ObtenerPresupuestoPermisos()
        {
            return Presupuesto * 0.20f;
        }

        public override string ObtenerTipo()
        {
            return "Monumento";
        }

        public override string ToString()
        {
            return $"Monumento ({Jeroglificos}) ({(Autoridad != "" ? Autoridad : "Sin autoridad")}) ({TributoDioses.Count} dioses)";
        }

        public override string ObtenerDetalles()
        {
            string dioses = "";
            for(int i=0;i<TributoDioses.Count;i++)
            {
                if (i != 0) dioses += ", ";
                dioses += TributoDioses[i];
            }

            return base.ObtenerDetalles() + "\n" +
                "Jeroglificos: " + Jeroglificos + "\n" +
                "Autoridad: " + (Autoridad != "" ? Autoridad : "No tiene") + "\n" +
                "Dioses: " + dioses + "\n";
        }
    }
}
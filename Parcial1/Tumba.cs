﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parcial1
{
    public class Tumba : Descubrimiento
    {
        private int _individuosEncontrados;
        public int IndividuosEncontrados {
            get { return _individuosEncontrados; }
        }

        private string _autoridad;
        public string Autoridad {
            get { return _autoridad; }
        }

        public Tumba(string pais, string ciudad, DateTime fecha,
            int individuosEncontrados, string autoridad) : base(pais, ciudad, fecha)
        {
            _individuosEncontrados = individuosEncontrados;
            _autoridad = autoridad;
        }

        public override float ObtenerPresupuestoExcavacion()
        {
            return Presupuesto * 0.50f;
        }

        public override float ObtenerPresupuestoMateriales()
        {
            return Presupuesto * 0.40f;
        }

        public override float ObtenerPresupuestoPermisos()
        {
            return Presupuesto * 0.10f;
        }

        public override string ObtenerTipo()
        {
            return "Tumba";
        }

        public override string ToString()
        {
            return $"Tumba ({IndividuosEncontrados}) ({(Autoridad != "" ? Autoridad : "Sin autoridad")})";
        }

        public override string ObtenerDetalles()
        {
            return base.ObtenerDetalles() + "\n" +
                "Individuos encontrados: " + IndividuosEncontrados + "\n" +
                "Autoridad: " + (Autoridad != "" ? Autoridad : "No tiene") + "\n";
        }
    }
}
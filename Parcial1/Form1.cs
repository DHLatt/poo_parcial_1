﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial1
{
    public partial class Form1 : Form
    {
        private Instituto instituto = new Instituto();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dateTimePickerNacimiento.MaxDate = DateTime.Now;
            dateTimePickerDeceso.MaxDate = DateTime.Now;

            comboBoxDescubrimiento.Items.Add("Ciudad");
            comboBoxDescubrimiento.Items.Add("Tumba");
            comboBoxDescubrimiento.Items.Add("Monumento");
            comboBoxDescubrimiento.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void ActualizarListaArqueologos()
        {
            listBoxArqueologos.DataSource = null;
            listBoxArqueologos.DataSource = instituto.Arqueologos;
        }

        private void ActualizarListaDescubrimientos()
        {
            listBoxDescubrimientos.DataSource = null;
            listBoxDescubrimientos.DataSource = instituto.Descubrimientos;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string apellido = textBoxApellido.Text;
            if (nombre == "" || apellido == "")
            {
                MessageBox.Show("Los campos de nombre y apellido no pueden estar vacios", "Error!");
                return;
            }


            DateTime fechaNacimiento = dateTimePickerNacimiento.Value;
            bool difunto = checkBoxDifunto.Checked;
            DateTime fechaDeceso = dateTimePickerDeceso.Value;
            if (difunto && fechaDeceso.Date <= fechaNacimiento.Date)
            {
                MessageBox.Show("La fecha de deceso es invalida", "Error!");
                return;
            }

            Arqueologo arqueologo = new Arqueologo(nombre, apellido, fechaNacimiento);
            if (difunto)
            {
                arqueologo.FechaDeceso = fechaDeceso;
            }
            instituto.Arqueologos.Add(arqueologo);

            // Limpiar campos
            textBoxNombre.Text = "";
            textBoxApellido.Text = "";
            dateTimePickerNacimiento.Value = dateTimePickerNacimiento.MaxDate;
            checkBoxDifunto.Checked = false;
            dateTimePickerDeceso.Value = dateTimePickerDeceso.MaxDate;

            ActualizarListaArqueologos();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerDeceso.Enabled = checkBoxDifunto.Checked;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Arqueologo arqueologo = (Arqueologo)listBoxArqueologos.SelectedItem;
            if (arqueologo == null)
            {
                MessageBox.Show("No hay un item de la lista seleccionado", "Error!");
            } else if (arqueologo.Descubrimientos > 0)
            {
                MessageBox.Show("No puede eliminarse un arqueologo que tiene descubrimientos", "Error!");
            } else
            {
                instituto.Arqueologos.Remove(arqueologo);
                ActualizarListaArqueologos();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Arqueologo arqueologo = (Arqueologo)listBoxArqueologos.SelectedItem;
            if (arqueologo == null)
            {
                MessageBox.Show("No hay un item de la lista seleccionado", "Error!");
            }
            else
            {
                MessageBox.Show(arqueologo.ObtenerDetalles(), "Arqueologo");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            Form formulario = null;

            string tipo = (string)comboBoxDescubrimiento.SelectedItem;
            switch(tipo)
            {
                case "Ciudad":
                    formulario = new FormNuevaCiudad(instituto);
                    break;
                case "Tumba":
                    formulario = new FormNuevaTumba(instituto);
                    break;
                case "Monumento":
                    formulario = new FormNuevoMonumento(instituto);
                    break;
            }

            if (formulario != null)
            {
                formulario.ShowDialog();
                ActualizarListaDescubrimientos();
            }
        }

        private void MostrarInformacionDescubrimiento(Descubrimiento descubrimiento)
        {
            if (descubrimiento == null)
            {
                MessageBox.Show("No hay ningun descubrimiento", "Error!");
            }
            else
            {
                MessageBox.Show(descubrimiento.ObtenerDetalles(), descubrimiento.ObtenerTipo());
            }
        }

        // d) Obtener el descubrimiento que haya obtenido más presupuesto en excavación
        private void button5_Click(object sender, EventArgs e)
        {
            Descubrimiento descubrimiento = instituto.ObtenerDescubrimientoMayorPresupuestoExcavacion();
            MostrarInformacionDescubrimiento(descubrimiento);
        }

        // e) Obtener el descubrimiento que haya asignado más presupuesto en materiales
        private void button6_Click(object sender, EventArgs e)
        {
            Descubrimiento descubrimiento = instituto.ObtenerDescubrimientoMayorPresupuestoMateriales();
            MostrarInformacionDescubrimiento(descubrimiento);
        }

        // f) Obtener el descubrimiento qué menos dinero haya abonado en permisos
        private void button7_Click(object sender, EventArgs e)
        {
            Descubrimiento descubrimiento = instituto.ObtenerDescubrimientoMenorPresupuestoPermisos();
            MostrarInformacionDescubrimiento(descubrimiento);
        }

        // g) Obtener el descubrimiento más antiguo
        private void button8_Click(object sender, EventArgs e)
        {
            Descubrimiento descubrimiento = instituto.ObtenerDescubrimientoMasAntiguo();
            MostrarInformacionDescubrimiento(descubrimiento);
        }

        // h) Obtener el arqueólogo qué más descubrimientos haya realizado
        private void button9_Click(object sender, EventArgs e)
        {
            Arqueologo arqueologo = instituto.ObtenerArqueologoMasDescubrimientos();
            if (arqueologo == null)
            {
                MessageBox.Show("No hay ningun arqueologo", "Error!");
            }
            else
            {
                MessageBox.Show(arqueologo.ObtenerDetalles(), "Arqueologo");
            }
        }

        private void buttonEliminar_Click(object sender, EventArgs e)
        {
            Descubrimiento descubrimiento = (Descubrimiento)listBoxDescubrimientos.SelectedValue;
            if (descubrimiento == null)
            {
                MessageBox.Show("No hay ningun descubrimiento seleccionado", "Error!");
            } else
            {
                instituto.EliminarDescubrimiento(descubrimiento);
                ActualizarListaDescubrimientos();
            }
        }

        private void buttonConsultar_Click(object sender, EventArgs e)
        {
            Descubrimiento descubrimiento = (Descubrimiento)listBoxDescubrimientos.SelectedValue;
            MostrarInformacionDescubrimiento(descubrimiento);
        }
    }
}

﻿
namespace Parcial1
{
    partial class FormNuevaTumba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxCantidadIndividuos = new System.Windows.Forms.TextBox();
            this.dateTimePickerFecha = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxAutoridad = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBoxArqueologos = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCiudad = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPais = new System.Windows.Forms.TextBox();
            this.checkBoxAutoridad = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPresupuesto = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 257);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(161, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Cantidad individuos encontrados";
            // 
            // textBoxCantidadIndividuos
            // 
            this.textBoxCantidadIndividuos.Location = new System.Drawing.Point(12, 276);
            this.textBoxCantidadIndividuos.Name = "textBoxCantidadIndividuos";
            this.textBoxCantidadIndividuos.Size = new System.Drawing.Size(237, 20);
            this.textBoxCantidadIndividuos.TabIndex = 34;
            // 
            // dateTimePickerFecha
            // 
            this.dateTimePickerFecha.Location = new System.Drawing.Point(12, 121);
            this.dateTimePickerFecha.Name = "dateTimePickerFecha";
            this.dateTimePickerFecha.Size = new System.Drawing.Size(237, 20);
            this.dateTimePickerFecha.TabIndex = 33;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 334);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Autoridad";
            // 
            // textBoxAutoridad
            // 
            this.textBoxAutoridad.Enabled = false;
            this.textBoxAutoridad.Location = new System.Drawing.Point(12, 350);
            this.textBoxAutoridad.Name = "textBoxAutoridad";
            this.textBoxAutoridad.Size = new System.Drawing.Size(237, 20);
            this.textBoxAutoridad.TabIndex = 27;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 391);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 35);
            this.button1.TabIndex = 26;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBoxArqueologos
            // 
            this.comboBoxArqueologos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxArqueologos.FormattingEnabled = true;
            this.comboBoxArqueologos.Location = new System.Drawing.Point(12, 171);
            this.comboBoxArqueologos.Name = "comboBoxArqueologos";
            this.comboBoxArqueologos.Size = new System.Drawing.Size(237, 21);
            this.comboBoxArqueologos.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Arqueologo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Fecha";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Ciudad";
            // 
            // textBoxCiudad
            // 
            this.textBoxCiudad.Location = new System.Drawing.Point(12, 77);
            this.textBoxCiudad.Name = "textBoxCiudad";
            this.textBoxCiudad.Size = new System.Drawing.Size(237, 20);
            this.textBoxCiudad.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Pais";
            // 
            // textBoxPais
            // 
            this.textBoxPais.Location = new System.Drawing.Point(12, 30);
            this.textBoxPais.Name = "textBoxPais";
            this.textBoxPais.Size = new System.Drawing.Size(237, 20);
            this.textBoxPais.TabIndex = 19;
            // 
            // checkBoxAutoridad
            // 
            this.checkBoxAutoridad.AutoSize = true;
            this.checkBoxAutoridad.Location = new System.Drawing.Point(12, 311);
            this.checkBoxAutoridad.Name = "checkBoxAutoridad";
            this.checkBoxAutoridad.Size = new System.Drawing.Size(100, 17);
            this.checkBoxAutoridad.TabIndex = 36;
            this.checkBoxAutoridad.Text = "Tiene autoridad";
            this.checkBoxAutoridad.UseVisualStyleBackColor = true;
            this.checkBoxAutoridad.CheckedChanged += new System.EventHandler(this.checkBoxAutoridad_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Presupuesto total";
            // 
            // textBoxPresupuesto
            // 
            this.textBoxPresupuesto.Location = new System.Drawing.Point(12, 225);
            this.textBoxPresupuesto.Name = "textBoxPresupuesto";
            this.textBoxPresupuesto.Size = new System.Drawing.Size(237, 20);
            this.textBoxPresupuesto.TabIndex = 37;
            // 
            // FormNuevaTumba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 437);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxPresupuesto);
            this.Controls.Add(this.checkBoxAutoridad);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxCantidadIndividuos);
            this.Controls.Add(this.dateTimePickerFecha);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxAutoridad);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxArqueologos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxCiudad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPais);
            this.Name = "FormNuevaTumba";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FormNuevaTumba";
            this.Load += new System.EventHandler(this.FormNuevaTumba_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxCantidadIndividuos;
        private System.Windows.Forms.DateTimePicker dateTimePickerFecha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxAutoridad;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBoxArqueologos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCiudad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPais;
        private System.Windows.Forms.CheckBox checkBoxAutoridad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPresupuesto;
    }
}
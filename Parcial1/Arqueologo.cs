﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parcial1
{
    public class Arqueologo
    {
        private string _nombre;
        public string Nombre
        {
            get { return _nombre; } 
        }

        private string _apellido;
        public string Apellido
        {
            get { return _apellido; }
        }

        private DateTime _fechaNacimiento;
        public DateTime FechaNacimiento
        {
            get { return _fechaNacimiento; }
        }

        private bool _difunto;
        public bool Difunto
        {
            get { return _difunto; }
        }

        private DateTime _fechaDeceso;
        public DateTime FechaDeceso
        {
            get { return _fechaDeceso; }
            set {
                _difunto = true;
                _fechaDeceso = value;
            }
        }

        private int _descubrimientos;
        public int Descubrimientos
        {
            get { return _descubrimientos; }
        }

        public Arqueologo(string nombre, string apellido,
            DateTime fechaNacimiento)
        {
            _nombre = nombre;
            _apellido = apellido;
            _fechaNacimiento = fechaNacimiento;
        }

        public void SumarDescubrimiento()
        {
            _descubrimientos++;
        }

        public void RestarDescubrimiento()
        {
            _descubrimientos--;
        }

        public string ObtenerDetalles()
        {
            string detalles = Nombre + " " + Apellido + "\n" +
                    "Fecha de nacimiento: " + FechaNacimiento.ToString("dd/MM/yyyy");
            if (Difunto)
            {
                detalles += "\n" + "Fecha de deceso: " + FechaDeceso.ToString("dd/MM/yyyy");
            }
            detalles += "\n" + "Descubrimientos: " + Descubrimientos;
            return detalles;
        }

        public override string ToString()
        {
            return Nombre + " " + Apellido;
        }
    }
}
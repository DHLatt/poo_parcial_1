﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial1
{
    public partial class FormNuevaTumba : Form
    {
        private Instituto _instituto;

        public FormNuevaTumba(Instituto instituto)
        {
            _instituto = instituto;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pais = textBoxPais.Text;
            string ciudad = textBoxCiudad.Text;
            bool tieneAutoridad = checkBoxAutoridad.Checked;
            string autoridad = textBoxAutoridad.Text;
            if (pais == "" || ciudad == "" || (tieneAutoridad && autoridad == ""))
            {
                MessageBox.Show("No puede haber campos vacios", "Error!");
                return;
            }

            DateTime fecha = dateTimePickerFecha.Value;

            Arqueologo arqueologo = (Arqueologo)comboBoxArqueologos.SelectedItem;
            if (arqueologo == null)
            {
                MessageBox.Show("El descubrimiento debe tener un arqueologo", "Error!");
                return;
            }

            try
            {
                int presupuesto = int.Parse(textBoxPresupuesto.Text);
                int cantidadIndividuos = int.Parse(textBoxCantidadIndividuos.Text);
                Descubrimiento descubrimiento = new Tumba(pais, ciudad, fecha, cantidadIndividuos, autoridad);
                descubrimiento.Arqueologo = arqueologo;
                descubrimiento.Presupuesto = presupuesto;
                _instituto.NuevoDescubrimiento(descubrimiento);
            }
            catch (Exception)
            {
                MessageBox.Show("Alguno de los campos numericos no es valido", "Error!");
                return;
            }

            Close();
        }

        private void FormNuevaTumba_Load(object sender, EventArgs e)
        {
            comboBoxArqueologos.DataSource = _instituto.Arqueologos;
            dateTimePickerFecha.MaxDate = DateTime.Now;
        }

        private void checkBoxAutoridad_CheckedChanged(object sender, EventArgs e)
        {
            textBoxAutoridad.Enabled = checkBoxAutoridad.Checked;
        }
    }
}

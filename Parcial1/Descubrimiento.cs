﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parcial1
{
    public abstract class Descubrimiento
    {
        private string _pais;
        public string Pais
        {
            get { return _pais; }
        }

        private string _ciudad;
        public string Ciudad
        {
            get { return _ciudad; }
        }

        private DateTime _fecha;
        public DateTime Fecha {
            get { return _fecha; }
        }

        private Arqueologo _arqueologo;
        public Arqueologo Arqueologo {
            get { return _arqueologo; }
            set { _arqueologo = value; }
        }

        private float _presupuesto;
        public float Presupuesto {
            get { return _presupuesto; }
            set { _presupuesto = value; }
        }

        public Descubrimiento(string pais, string ciudad, DateTime fecha)
        {
            _pais = pais;
            _ciudad = ciudad;
            _fecha = fecha;
        }

        public abstract float ObtenerPresupuestoExcavacion();

        public abstract float ObtenerPresupuestoMateriales();

        public abstract float ObtenerPresupuestoPermisos();

        public abstract string ObtenerTipo();

        public virtual string ObtenerDetalles()
        {
            return "Pais: " + Pais + "\n" +
                "Ciudad: " + Ciudad + "\n" +
                "Fecha:" + Fecha + "\n" + 
                "Arqueologo: " + Arqueologo + "\n" +
                "Presupuesto: $" + Presupuesto + "\n" +
                "Presupuesto Excavacion: $" + ObtenerPresupuestoExcavacion() + "\n" +
                "Presupuesto Materiales: $" + ObtenerPresupuestoMateriales() + "\n" +
                "Presupuesto Permisos: $" + ObtenerPresupuestoPermisos() + "\n";
        }
    }
}
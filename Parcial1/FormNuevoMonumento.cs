﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial1
{
    public partial class FormNuevoMonumento : Form
    {
        private Instituto _instituto;

        public FormNuevoMonumento(Instituto instituto)
        {
            _instituto = instituto;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string pais = textBoxPais.Text;
            string ciudad = textBoxCiudad.Text;
            bool jeroglificos = checkBoxJeroglificos.Checked;
            bool tieneAutoridad = checkBoxAutoridad.Checked;
            string autoridad = textBoxAutoridad.Text;
            List<string> dioses = new List<string>();
            foreach (string nombre in listBoxDioses.Items)
            {
                dioses.Add(nombre);
            }

            if (pais == "" || ciudad == "" || (tieneAutoridad && autoridad == ""))
            {
                MessageBox.Show("No puede haber campos vacios", "Error!");
                return;
            }

            DateTime fecha = dateTimePickerFecha.Value;

            Arqueologo arqueologo = (Arqueologo)comboBoxArqueologos.SelectedItem;
            if (arqueologo == null)
            {
                MessageBox.Show("El descubrimiento debe tener un arqueologo", "Error!");
                return;
            }

            try
            {
                int presupuesto = int.Parse(textBoxPresupuesto.Text);
                Descubrimiento descubrimiento = new Monumento(pais, ciudad, fecha, jeroglificos, autoridad, dioses);
                descubrimiento.Arqueologo = arqueologo;
                descubrimiento.Presupuesto = presupuesto;
                _instituto.NuevoDescubrimiento(descubrimiento);
            }
            catch (Exception)
            {
                MessageBox.Show("Alguno de los campos numericos no es valido", "Error!");
                return;
            }

            Close();
        }

        private void FormNuevoMonumento_Load(object sender, EventArgs e)
        {
            comboBoxArqueologos.DataSource = _instituto.Arqueologos;
            dateTimePickerFecha.MaxDate = DateTime.Now;
        }

        private void buttonDiosAgregar_Click(object sender, EventArgs e)
        {
            string dios = textBoxDios.Text;
            if (dios != "")
            {
                listBoxDioses.Items.Add(dios);
                textBoxDios.Text = "";
            } else
            {
                MessageBox.Show("El nombre del dios no puede estar vacio", "Error!");
            }
        }

        private void buttonDiosEliminar_Click(object sender, EventArgs e)
        {
            object item = listBoxDioses.SelectedItem;
            if (item != null)
            {
                listBoxDioses.Items.Remove(item);
            } else
            {
                MessageBox.Show("No hay ningun item seleccionado", "Error!");
            }
        }

        private void checkBoxAutoridad_CheckedChanged_1(object sender, EventArgs e)
        {
            textBoxAutoridad.Enabled = checkBoxAutoridad.Checked;
        }
    }
}
